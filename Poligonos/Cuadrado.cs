﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poligonos
{
    public class Cuadrado
    {
        /// <summary>
        /// Recoge el valor del lado del cuadrado
        /// </summary>
        public double Lado { get; set; }
        /// <summary>
        /// Calcula el area del cudrado
        /// </summary>
        public double Area { get => Lado * Lado; }
    }
}
